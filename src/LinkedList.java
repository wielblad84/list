public class LinkedList <T>{


    private Node first;
    private Node last;
    private int size = 0;

    public boolean isEmpty() {

        return size == 0;
    }
    public int size() {

        return size;
    }


    public void addFirstElement(T t) {
        Node n = new Node(t, first, null);
        if (first != null) {
            first.previous = n;
        }
        first = n;
        if (last == null) {
            last = n;
        }
        size++;

    }

    public void addLastElement(T t) {
        Node n = new Node(t, null, last);
        if (last != null) {
            last.next = n;
        }
        last = n;
        if (first == null) {
            first = n;
        }
        size++;

    }


    public T get(T t) {
        Node current = first;
        while (current != null) {
            if (t.equals(current.value))
                return current.value;
            current = current.next;
        }
        return null;
    }
    public void forward(){
        Node n=first;
        while(n!=null){
            n=n.next;
        }
    }
    public  void backward() {
        Node n = last;
        while (n != null) {
            n = n.previous;
        }
    }

    class Node {
        T value;
        Node next;
        Node previous;

        public Node(T value, Node next, Node previous) {
            this.value = value;
            this.next = next;
            this.previous = previous;
        }
    }
}


